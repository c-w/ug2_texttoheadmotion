<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <!-- Using stylesheets from the "Better Web Readability Project"
         http://code.google.com/p/better-web-readability-project/ -->
    <link rel="stylesheet" href="reset.css" type="text/css" media="screen, projection">
    <link rel="stylesheet" href="typography.css" type="text/css" media="screen, projection">
    <link rel="stylesheet" href="report.css" type="text/css" media="screen, projection">
    <title>Report</title>
</head>

<body>
    <div class="main">
        <div class="header">
            <h1>Predicting Head Motion from Text</h1>
            <div class="authors">Clemens Wolff<a href="#Note1" class="note" num="1"></a>, Hiroshi Shimodaira<a href="#Note2" class="note" num="2"></a></div>
            <div class="institutions"><a name="Note1" class="note" num="1"></a><a href="http://www.inf.ed.ac.uk/">School of Informatics</a>, <a href="http://www.ed.ac.uk/">University of Edinburgh, UK</a>, <a name="Note2" class="note" num="2"></a><a href="http://www.cstr.ed.ac.uk/">CSTR</a>, <a href="http://www.ed.ac.uk/">University of Edinburgh, UK</a></div>
            <div class="emails">&#115;&#48;&#57;&#52;&#50;&#50;&#56;&#52;&#64;&#115;&#109;&#115;&#46;&#101;&#100;&#46;&#97;&#99;&#46;&#117;&#107;, &#104;&#46;&#115;&#104;&#105;&#109;&#111;&#100;&#97;&#105;&#114;&#97;&#64;&#101;&#100;&#46;&#97;&#99;&#46;&#117;&#107;</div>
        </div>

        <a name="Section1"></a>
        <div class="section">
            <h2>1. Motivation</h2>
            <p>Lifelike conversational agents that capture human behaviour
            (gestures, movement, etc.), engage humans, and interact with
            them in conversation are one of the potential future-generation
            human-computer interfaces. Alternatively, the ability to
            automatically animate such models can be used to reduce the cost of
            computer animation (movies, games, etc.) by eliminating the
            need to generate extensive motion-capture databases.</p>
            <p>Many advances have been made to make conversational agents
            natural, especially in the domains of mouth-, lip-, and
            eye-movements. However, relatively little research into modelling
            and automatically generating the movement of the head itself have
            been made - despite the importance of head-motion in speech
            perception <a href="#Reference1">[1]</a> and conversational
            flow <a href="#Reference2">[2]</a> having been established.</p>
            <p>This report explores the design of a proof-of-concept
            head-motion synthesis system. The system independently learns
            characteristic types of head movements and uses these to predict
            head-motion trajectories from text. First the motions are
            discretized. Then, linear chain Conditional Random Field (CRF)
            and Hidden Markov Model (HMM) based approaches are used to learn
            the relations between the head-motions and a number of features
            extracted from the words in the input text. The use of a design
            based on CRFs is justified by the success of this technique in the
            domain of part-of-speech tagging, which, in essence, can be seen as
            a similar problem: finding the mapping between two discrete streams
            of tokens. The HMM model was implemented to investigate the
            influence of the underlying classification paradigm on prediction
            accuracy. Using both subjective and quantitative evaluation, it was
            found that a low-dimensional feature-set and an HMM-based model
            lead to head-motion predictions that look reasonably natural.</p>
            <p>Previous work used a unit-selection based approach to learn the
            mapping between text and head movements
            <a href="#Reference3">[3]</a> or focused on generating head motion
            from speech <a href="#Reference4">[4]</a>. This system attempts to
            directly learn head-motion patterns from raw data describing the
            movement of a head in three dimensions (Euler angles), and map
            these to input text.</p>
            <p>The remainder of this report is structured as follows:
            <a href="#Section2">Section 2</a> gives an overview of a baseline
            system for automated text-to-head-motion synthesis. The system's
            performance is evaluated, explained, and discussed in
            <a href="#Section3">Section 3</a>.
            <a href="#Section4">Section 4</a> presents and discusses a number
            of improvements to the baseline system.
            <a href="#Section5">Section 5</a> summarizes the findings, explores
            possible future enhancements to the system, proposes some other
            approaches to be explored, and concludes the report.</p>
        </div>

        <a name="Section2"></a>
        <div class="section">
            <h2>2. Method</h2>

            <a name="Section2.1"></a>
            <div class="subsection">
                <h3>2.1. Data Preparation</h3>
                <p>A previously created database of recorded speech and
                matching Euler angle data <a href="#Reference5">[5]</a> was
                adapted  as follows to fit the requirements of this project:</p>
                <p>Seven pieces of recorded free speech from a single speaker,
                each two minutes long, were annotated with word-information
                and sentence boundaries. Forced alignment and manual
                segmentation were employed to find the true start- and
                end-times of each word, giving the first input of the system: a
                discrete stream of word tokens and timing information.</p>
                <p>The corresponding continuous Euler angle information was
                normalised to zero-mean, and split into frames using a sliding
                window of length 100 milliseconds (ms) with an overlap of 50
                ms. <a href="figure/sliding_window.html">Figure 1</a>
                illustrates this process. Using vector quantisation (VQ)
                (<a href="algorithm/vq_algorithm.html">Algorithm 1</a>), two sets
                of codebooks of representative unit head motions were extracted
                from these frames: one with 8 elements and one with 16
                elements. Each frame was then assigned to one of the codebooks
                according to a nearest-neighbour metric. This gives a second
                stream of discrete information: tokens representing one of 8 or
                16 different unit head motions. Each word has thus been
                labelled with exactly one of 8 or 16 unit head motions.</p>
                <p>The choice of codebook sizes was justified by balancing the
                clustering error introduced by reducing the true frames to
                unit-codebooks (as given by
                <a href="formula/error_formula.html">Formula 1</a>) against
                the hypothesis that too large a codebook size could lead to
                using labels detached from real-world expressiveness. Previous
                research suggests that all head motion can be classified as one
                of four main types <a href="#Reference6">[6]</a> - this is a
                strong argument in favour of aiming for a system employing
                fewer labels.
                <a href="figure/codebook_size_justification.html">Figure 2</a>
                plots the aforementioned introduced error for various codebook
                sizes. An examination of the figure reveals that increasing the
                size of the codebooks beyond 8 or 16 only gives diminishing
                returns in error reduction, thence undermining the validity of
                the choice of either of these two numbers for codebook size.
                </p>
                <p>The choice of frame length, however, is much harder to
                justify quantitatively.
                <a href="figure/frame_length_tradeoff.html">Figure 3</a>
                shows that there exists a relation between frame length and
                clustering error. The figure also reveals that there exists a
                relation between frame length and expressiveness of the
                resulting unit head motions. The longer the frames, the more
                structure and distinctiveness the extracted unit head motions
                exhibit. An increase in frame length, however, also entails a
                higher error in clustering. Furthermore, due to the limited
                amount of data available to this project, longer frames also
                have the potential of introducing 'jerks' to the unit head
                motions. Long frame lengths imply less Euler angle vectors, and
                since VQ is essentially a clustering technique, having less
                data can lead to some of the final unit head motions being
                representative for only a few vectors. This increases variance
                along the unit head motion, and therewith explains the observed
                unsmoothness. While the correct choice of frame length remains
                a topic of debate, this project deems that the data sparsity
                related 'jerks', the increase in clustering error, and the
                general intuition that longer, distinctive, and expressive head
                motion patterns should be recoverable through reconstruction
                from shorter fragments, are sufficiently good arguments to
                justify opting for a short frame length.</p>
                <p>Finally, the word-token stream and codebook stream were
                synchronised using the word-timing information previously
                found: for each word, all the frames overlapping with its
                duration were found, the codebooks corresponding to the frames
                were retrieved, and whichever among these labels appeared the
                most often was chosen to be the representative codebook for
                this word (majority-vote criterion).
                <a href="figure/word_codebook_alignment.html">Figure 4</a>
                illustrates this process.</p>
                <p>Each of the seven pieces of data for the system has thus
                been converted into a discrete sequence of sentences. Each
                sentence consists of pairs of words and corresponding codebooks
                extracted from Euler angle data. The so-processed data was
                split into a training set of six elements, and a validation set
                of one element. A summary overview of the entire process is
                given by
                <a href="figure/data_preparation_all.html">Figure 5</a>.</p>
            </div>

            <a name="Section2.2"></a>
            <div class="subsection">
                <h3>2.2. System Overview</h3>
                <p><a href="figure/system_overview.html">Figure 6</a> gives a
                high-level overview of the proposed text-to-head-motion
                synthesis system.</p>
                <p>To train the system, two streams of data are used: text, and
                for each word in the text, a codebook representing a
                unit head-motion associated with this word. A text-to-speech
                tool <a href="#Reference7">[7]</a> is used to extract six
                features from each word: word token, part-of-speech tag,
                duration, prosody, position within its wrapping sentence, and
                what sort of phrase break may follow the word. Duration and
                prosody features are discretized into three categories
                according to their magnitude of deviation from the
                feature-mean: high-positive deviation, high-negative deviation,
                and close to the mean. The position-in-sentence feature is also
                discretized into three categories: first 10% of the sentence,
                centre, and last 10% of the sentence. The phrase break
                information output of the text-to-speech system is kept as is,
                namely features belonging to one of three classes: long break,
                short break, and no break. Using these word/codebook/features
                tuples, an off-the-shelf tool-kit <a href="#Reference8">[8]</a>
                is then used to train the three different systems based on
                linear chain CRFs
                (<a href="formula/crf_formula.html">Formula 2</a>) described
                in <a href="#Section2.3">Section 2.3</a>,
                <a href="#Section2.4">Section 2.4</a>, and
                <a href="#Section2.5">Section 2.5</a>.</p>
                <p>To classify a text with the system, the same six features
                as during training are extracted from the text. These features
                are then fed into the previously trained systems, which output
                a codebook label for each word in the input text. Recall that
                each codebook label corresponds to a frame, or vector, of Euler
                angles - a head-motion trajectory can thus easily be
                re-synthesised from the output sequence of codebook labels by
                stretching the labels to cover the duration of the words and
                interpolating between words to give smooth transitions.</p>
            </div>

            <a name="Section2.3"></a>
            <div class="subsection">
                <h3>2.3. 1-CRF System</h3>
                <p>For each word, its features, the features of the previous
                word, and the features of the next word are concatenated into
                an 18-dimensional vector. A linear chain CRF is trained to find
                the mapping between these vectors and the associated
                codebooks.</p>
            </div>

            <a name="Section2.4"></a>
            <div class="subsection">
                <h3>2.4. 6-CRF System</h3>
                <p>An off-the-shelf tool-kit <a href="#Reference9">[9]</a> is
                used to perform feature selection to find how important each of
                the six features types is when it comes to predicting codebook
                labels (using an information gain criterion). For each word,
                and for each type of feature, the features of that word, the
                previous word, and the next word, are concatenated into a
                3-dimensional vector. One linear chain CRF is trained to find
                the mapping between each of these streams of vectors and the
                associated codebooks. Each CRF gets a weight associated with it
                that is proportional to its information gain score established
                earlier.</p>
                <p>To classify a text, each of the six CRFs so-trained is used
                independently to predict a codebook label - thus, for each word
                in the text to classify, six labels are suggested. Then, a
                loaded die with six faces is rolled to determine which of these
                suggested labels to choose as the actual prediction result. Each
                face of the die shows one of the six labels suggested by the
                six CRFs, and has a probability proportional to the weight of
                that CRF to be rolled. This gives, in essence, a very crude
                mixture-of-experts type model.</p>
                <p>This system was implemented to investigate possible data
                over-fitting issues that could arise due to the high
                dimensionality of the feature-vectors in the system proposed in
                <a href="#Section2.3">Section 2.3</a>.</p>
            </div>

            <a name="Section2.5"></a>
            <div class="subsection">
                <h3>2.5. 18-CRF System</h3>
                <p>This system is very similar to the design proposed in
                <a href="#Section2.4">Section 2.4</a>, however, the
                dimensionality of the feature-vectors is reduced further: each
                of the six CRFs in the system of
                <a href="#Section2.4">Section 2.4</a> is 'split' into three
                smaller CRFs: one for the previous features, one for the
                current features, and one for the next features. The input
                vectors are thus 1-dimensional, 18 CRFs are trained, input text
                is classified with each of these, and an 18-sided loaded die is
                rolled to determine which of the 18 predicted labels to
                choose.</p>
            </div>
        </div>

        <a name="Section3"></a>
        <div class="section">
            <h2>3. Results and Discussion</h2>
            <p>Preliminary qualitative evaluation of the system revealed that
            using 8 target labels rather than 16 improved the performance of
            the system on validation data five-fold, while not significantly
            affecting the system's performance when evaluated on data seen
            during training. The remainder of this report shall thus focus on
            the systems using a codebook size of 8 unit head motions.</p>
            <p>A comparison of the performances of the three proposed systems
            on both seen and unseen data is given by
            <a href="figure/all_features_accuracy.html">Figure 7</a>. The
            evaluation metric used is a simple confusion-matrix accuracy
            rating: a system's performance is quantified as how often it
            predicted exactly the correct codebook out of the total number of
            predictions made. While this method may be overly harsh due to
            working with absolutes and thus ignoring the possibility of some
            codebooks representing similar trajectories, this project deems it
            a good enough baseline evaluation method.
            <a href="figure/all_features_confusion.html">Figure 8</a> breaks
            up the classification results and gives detail on which labels were
            predicted most accurately on average. If label X has been predicted
            as label Y N times, the entry in the table in column X row Y will
            be blue if N is small, red if N is large, or grey if this pair
            of prediction/classification results has not occurred. A good
            performance of the system would lead to a concentration of shades
            of red along the principal diagonal of the matrix.</p>
            <p>The 1-CRF system performs almost perfectly when classifying data
            seen during training. On the other hand, it exhibits a performance
            significantly below random-baseline level when classifying entirely
            new data. This, alongside the large feature-vector dimension, and
            lack of training data, points toward the possibility of an
            over-fitted system. However, when compared to the 6-CRF and 18-CRF
            systems, both of which use much lower feature-vector sizes, no
            significant improvement when classifying unseen data can be seen.
            As a matter of fact, reducing the feature-vector dimension from 18
            (1-CRF system) to 1 (18-CRF system) only leads to a 16% relative or
            1% absolute improvement of classification results on new data. This
            weakens the hypothesis that the poor performance of the proposed
            system is mainly due to data sparsity related over-fitting.</p>
            <p>An investigation into the data scarcity hypothesis revealed that
            the  most relevant features for predicting codebook labels, as
            ranked by the information gain criterion introduced in
            <a href="#Section2.4">Section 2.4</a>, are also the most scarce in
            the training data. Word information is the most discriminative
            feature, concentrating more than 50% of the relative predictive
            power, but less than 5% of the words in the validation set were
            seen during training. Part-of-speech information is the second most
            important feature, and accounts for another 30% of the total
            predictive importance mass, but less than 60% of the part-of-speech
            tags in the validation set were seen during training. This mismatch
            between training and validation set is introduced by the fact that
            this project's data-set is small and consists of free speech on
            rather different topics. To investigate the possible influence of
            this factor on the system's classification results, training and
            classification were repeated ignoring word and part-of-speech
            information.
            <a href="figure/ignore_features_accuracy.html">Figure 9</a> plots
            the accuracies of the resulting classifiers. While the overall
            performance of the classifiers remains very poor and in sub
            random-baseline domains, the trends shown by these plots support
            the hypotheses established earlier. Removing the scarce word and
            part-of-speech features improves the 1-CRF system's performance by
            63% in relative or 4% in absolute terms, validating the assumption
            of data scarcity being a problem. Furthermore, the finding that
            word and part-of-speech information are the most important features
            when trying to predict head motion is strengthened by the fact that
            removing these two features makes the performance of the 6-CRF and
            18-CRF systems disintegrate. The visualisations of the confusion
            matrices in
            <a href="figure/ignore_features_confusion.html">Figure 10</a>
            further support this point by revealing a complete loss of
            structure in this second set of classifications, especially when
            using the 6-CRF and 18-CRF systems.</p>
        </div>

        <a name="Section4"></a>
        <div class="section">
            <h2>4. Improvements</h2>
            <p>The design proposed in <a href="#Section2">Section 2</a> and
            conclusions reached in <a href="#Section3">Section 3</a> suggest a
            number of avenues that could improve on the system's performance.
            Chiefly among these figure:</p>
            <p>1) Replacing the VQ-based head motion labels with labels created
            by a model with fewer underlying assumptions that are hard to
            justify.</p>
            <p>2) Investigating whether or not this project's choice of basing
            its system on CRFs was sound. So far this choice is only justified
            intuitively, and by analogy with other applications matching
            streams of discrete symbols.</p>
            <p>3) Using a reduced feature-set to further investigate the effect
            of feature-sparsity.</p>
            <p>Implementing changes 2) and 3) individually allowed for
            improvements on the performance reported in
            <a href="#Section3">Section 3</a> to be achieved. Implementing all
            three changes simultaneously led to a system performing reasonably
            well in both subjective and quantitative evaluations.</p>

            <a name="Section4.1"></a>
            <div class="subsection">
                <h3>4.1. Changes to the System</h3>
                <p>To investigate avenue 1), head motion clusters found by
                <a href="#Reference10">[10]</a> using a Gaussian Mixture Model
                (GMM) were employed. Some mild pre-processing was necessary to
                fit the data to the needs of this project. Using the provided
                label-time-alignment information together with previously found
                word-time-alignment information, the head-motion label
                intervals were mapped down to individual word-level. Thence
                again: each word was assigned with one of eight different
                unit-head motions.</p>
                <p>To replace the CRF-predictor, as suggested by improvement
                2), an HMM-based model was implemented as follows. For each of
                the eight codebook labels individually, a 3-state HMM was
                trained on the feature-vectors tagged with that label. Training
                was done using the Baum-Welch implementation provided by
                <a href="#Reference11">[11]</a>. After the training, emission
                and transition probabilities were smoothed to avoid
                zero-probability related problems. The resulting eight HMMs
                were then combined into one big 24-state HMM as shown in
                <a href="figure/hmm_design.html">Figure 11</a>. The resulting
                big HMM was then trained on the entire training set, discarding
                any labelling information. After this second-pass training,
                emission and transition probabilities were, once again,
                smoothed. To predict a sequence of codebook labels from a text,
                the Viterbi-path through the big HMM is computed. The resulting
                sequence of states gives the desired sequence of codebook
                labels.</p>
                <p>Exploring change 3) was done by modifying the six previously
                defined features extracted from each word in the input texts
                by: discarding the very scarce word-token information
                completely, and condensing the original part-of-speech tag-set
                of 36 elements down to 9 elementary tags ('noun', 'pronoun',
                'verb', 'adjective', 'adverb', 'preposition', 'conjunction',
                'interjection', and 'other'). These two changes reduce the
                dimension of the feature-set by two orders of magnitude.</p>
            </div>

            <a name="Section4.2"></a>
            <div class="subsection">
                <h3>4.2. Results and Discussion</h3>
                <p>There was no need to control for the impact of the new GMM
                cluster labels: the systems proposed in this report simply find
                mappings between a stream of vectors and a stream of arbitrary
                numbers: the systems are blind to the exact method of
                generation of these numbers. Whether the labels stem from a
                VQ- or a GMM-based model is therefore inconsequential to the
                performance of the systems.</p>
                <p><a href="figure/new_features_accuracy.html">Figure 12</a>
                uses the previously defined confusion-matrix based accuracy
                rating to compare the performance of the 1-CRF system to the
                accuracy of the new HMM based system, both using the new
                feature-set introduced in
                <a href="#Section4.1">Section 4.1</a>. From this figure, it is
                evident that the choice of features is a major contributor to
                the performance of a head-motion prediction system: using the
                new features improves the 1-CRF-system's performance on unseen
                data three-fold (12% absolute improvement). This is due to the
                fact that the new feature-set is much smaller (729 possible
                vectors) than the previous feature-set. This leads to a smaller
                proportion of instance not being seen during training and
                therewith a higher accuracy when classifying. Furthermore, the
                figure reveals that the HMM-based system performs 14% better on
                unseen data than the CRF-based system (3% absolute
                improvement). This validates the previously raised point that
                the choice of underlying classifier-model needs to be justified
                more rigorously.</p>
                <p>To further investigate the difference in performance between
                the HMM- and CRF-based systems, a new accuracy score based on
                the Jaro sequence similarity measure
                (<a href="formula/jaro_formula.html">Formula 3</a>) was
                implemented. The performances of the HMM- and CRF-based systems
                under this new metric are given by
                <a href="figure/new_features_jaro.html">Figure 13</a>. An
                examination of this plot reveals that, using this new metric,
                the difference in performance between the two models is made
                more prominent. The HMM-based system is assigned a reasonably
                good score of 72% accuracy on unseen data. The CRF-based
                system, on the other hand, only manages to achieve an accuracy
                of 42% on new data. This finding might be caused by the fact
                that the CRF-based system tends to produce longer runs of the
                same head-motion, whereas the HMM-based system's output is more
                varied.</p>
                <p>Re-synthesising trajectories from the head-motion labels
                output by the two systems allowed for a subjective evaluation
                to be carried out. The results of this evaluation were
                consistent with the findings of the quantitative evaluation.
                Participants reported that the outputs of the HMM-base system
                looked quite natural, definitely better than the productions of
                the CRF-base system. However, due to the very limited scale of
                this evaluation, the insights gained from the effort can only be
                considered anecdotal.</p>

                <center>
                <iframe width="480" height="360" frameborder="0" allowfullscreen
                    src="http://www.youtube.com/embed/wkj49Al3agA?rel=0">
                </iframe>
                <iframe width="480" height="360" frameborder="0" allowfullscreen
                    src="http://www.youtube.com/embed/1xBHA6KfDKA?rel=0">
                </iframe>
                </center>
            </div>
        </div>

        <a name="Section5"></a>
        <div class="section">
            <h2>5. Conclusion and Future Work</h2>
            <p>This report proposed a design for a head motion synthesiser. The
            system learns the mapping from text to 8 different unit head
            motions, and uses this function to predict and re-synthesise new
            head movement trajectories from any given input text. The system
            was implemented by training a CRF-based model and an HMM-based
            model on two different feature-sets extracted from text using a
            text-to-speech tool. It was found that the set of features used has
            a major impact on the accuracy of the predictions. Furthermore, it
            was unexpectedly found that the HMM-based classifier produced more
            accurate predictions and re-synthesised head motions that looked
            'more natural' than the CRF-based system.</p>
            <p>Possible future work could include:
            <p>1) Training the system on a larger data-set to further explore
            the hypothesised influence of data sparsity on the system's
            performance.</p>
            <p>2) Using more task-appropriate HMM flavour than the current
            generic system. Due to the nature of this system's inputs, a
            factorised HMM could, for instance, be a promising architecture.</p>
            <p>3) Rigorously investigating which features of words are
            correlated with head movements. This would allow to reduce the
            feature-set dimensionality and therewith increase the precision of
            the systems.</p>
            <p>4) Employing a more sophisticated method of learning unit head
            motions from the raw Euler angle data. While the fixed frame-length
            VQ approach or GMM-based approach of this project are simple and
            easy to understand models, they might not be able to capture the
            full complexity of real head motions, and implicitly make the
            assumption that most words have a single characteristic head motion
            that can be associated with them. Possible more relaxed
            alternatives include exploring word-based dynamic time warping to
            allow for variability in head motion lengths. Furthermore, taking
            into account higher-order features of the Euler angles, such as
            velocity and acceleration, could potentially help discriminate more
            characteristic unit head motions.</p>
            <p>5) Tackling the lack of word token information. This could be
            achieved by not using the original word tokens themselves, which
            will always be bound to be rather scarce unless the training set
            encompasses the entire vocabulary of the English language. A
            possible replacement could be an intermediate form, between word
            and part-of-speech tag, or grouping words into semantic groups such
            as 'assertive word', or 'negative word'.</p>
        </div>

        <a name="References"></a>
        <div class="section">
            <h2>References</h2>
            <a name="Reference1"></a><div class="reference">
                [1]
                <span class="authors">K. G. Munhall, J. A. Jones, D. E. Callan, T. Kuratate, E. Vatikiotis-Bateson</span>
                <span class="title">Visual prosody and speech intelligibility: head movement improves auditory speech perception</span>
                <span class="publication">Psychological Science 15</span>
                <span class="year">2004</span>
            </div>
            <a name="Reference2"></a><div class="reference">
                [2]
                <span class="authors">U. Hadar, T. J. Steiner, F. Clifford Rose</span>
                <span class="title">Head movement during listening turns in conversation</span>
                <span class="publication">Journal of Nonverbal Behavior 9</span>
                <span class="year">1985</span>
            </div>
            <a name="Reference3"></a><div class="reference">
                [3]
                <span class="authors">K. Liu, J. Ostermann</span>
                <span class="title">Realistic head motion synthesis for an image-based talking head</span>
                <span class="publication">2011 IEEE International Conference on Automatic Face Gesture
                Recognition and Workshops</span>
                <span class="year">2011</span>
            </div>
            <a name="Reference4"></a><div class="reference">
                [4]
                <span class="authors">C. Busso, Z. Deng, M. Grimm, U. Neumann, S. Narayanan</span>
                <span class="title">Rigid Head Motion in Expressive Speech Animation: Analysis and Synthesis</span>
                <span class="publication">Press IEEE Transactions on Audio, Speech and Language Processing</span>
                <span class="year">2007</span>
            </div>
            <a name="Reference5"></a><div class="reference">
                [5]
                <span class="authors">G. Hofer, H. Shimodaira</span>
                <span class="title">Automatic head motion prediction from speech data</span>
                <span class="publication">Proc. Interspeech</span>
                <span class="year">2007</span>
            </div>
            <a name="Reference6"></a><div class="reference">
                [6]
                <span class="authors">U. Hadar, T. J. Steiner, E. C. Grant, F. Clifford Rose</span>
                <span class="title">Kinematics of head movements accompanying speech during conversation</span>
                <span class="publication">Human Movement Science 2</span>
                <span class="year">1983</span>
            </div>
            <a name="Reference7"></a><div class="reference">
                [7]
                <span class="authors">A. W. Black, R. Clark, K. Richmond, J. Yamagishi, V. Strom, S. King</span>
                <span class="title">The Festival Speech Synthesis System</span>
                <span class="publication"><a href="http://www.cstr.ed.ac.uk/projects/festival/">http://www.cstr.ed.ac.uk/projects/festival</a></span>
                <span class="year">2010</span>
            </div>
            <a name="Reference8"></a><div class="reference">
                [8]
                <span class="authors">A. K. McCallum</span>
                <span class="title">MALLET: A Machine Learning for Language Toolkit</span>
                <span class="publication"><a href="http://mallet.cs.umass.edu">http://mallet.cs.umass.edu</a></span>
                <span class="year">2002</span>
            </div>
            <a name="Reference9"></a><div class="reference">
                [9]
                <span class="authors">M. Hall, E. Frank, G. Holmes, B. Pfahringer, P. Reutemann, I. H. Witten</span>
                <span class="title">The WEKA Data Mining Software: An Update</span>
                <span class="publication">SIGKDD Explorations, Volume 11, Issue 1</span>
                <span class="year">2009</span>
            </div>
            <a name="Reference10"></a><div class="reference">
                [10]
                <span class="authors">A. Ben Youssef, H. Shimodaira</span>
                <span class="title">Head Motion Clustering Using Gaussian Mixture Models</span>
                <span class="publication">Unpublished</span>
                <span class="year">2012</span>
            </div>
            <a name="Reference11"></a><div class="reference">
                [11]
                <span class="authors">A. Schliep, B. Georgi, W. Rungsarityotin, I. G. Costa, A. Sch�nhuth</span>
                <span class="title">The General Hidden Markov Model Library: Analyzing Systems with Unobservable States</span>
                <span class="publication">Proceedings of the Heinz-Billing-Price</span>
                <span class="year">2004</span>
            </div>
        </div>
    </div>
</body>
</html>
