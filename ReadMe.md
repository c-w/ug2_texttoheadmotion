## Predicting Head Motion from Text

Supervisor: [Dr. Hiroshi Shimodaira](http://homepages.inf.ed.ac.uk/hshimoda/),
[Centre for Speech Technology Research](http://www.cstr.ed.ac.uk/),
[University of Edinburgh](http://www.inf.ed.ac.uk/)

For more detail on the approach, refer to the full report (index.html).
The following gives a brief summary of the paper.

### Data
- Time-aligned transcriptions of free speech.
- Representations of the motion of the head in three dimensions, recorded during
  the uttering of the free speech.

### Techniques
- Clustering of the continuous representation of the motion of the head to find
  unitary head motions.
- Mapping of words to unitary head motions.
- Learning of the discrete-to-discrete mapping between feature-vectors (word
  token, part-of-speech, prosody, ...) and unitary head motions.
- Extraction of feature-vectors from input text using text-to-speech technology
  during prediction.

### Findings
- Of the explored classifiers, Hidden Markov Models work best.
- A better way to find unitary head motions is needed.
